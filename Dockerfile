FROM ruby:3.1.0

WORKDIR /app

RUN apt-get update && \
    apt-get install -y \
      build-essential \
      nodejs \
      yarn

RUN curl -fsSL https://deb.nodesource.com/setup_16.16.0 | sudo -E bash - &&\
sudo apt-get install -y nodejs

COPY Gemfile Gemfile.lock ./

RUN bundle install

COPY . .

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]
