class Tweet < ApplicationRecord
  belongs_to :user

  BODY_SIZE = ENV['TWEET_BODY_SIZE'] || 180

  before_create :check_size
  before_create :check_body_duplicate

  protected

  def check_size
    raise ActiveRecord::RecordInvalid.new if body.size > BODY_SIZE
  end

  def check_body_duplicate
    raise ActiveRecord::RecordInvalid.new if self.user.tweets.where(body: body).present?
  end
end
